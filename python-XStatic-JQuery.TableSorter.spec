%global _empty_manifest_terminate_build 0
Name:		python-XStatic-JQuery.TableSorter
Version:	2.14.5.2
Release:	1
Summary:	JQuery.TableSorter 2.14.5 (XStatic packaging standard)
License:	MIT
URL:		https://github.com/Mottie/tablesorter
Source0:	https://files.pythonhosted.org/packages/1b/6b/1b70ce0d8a7fdff86f6793ff08f6dced1d31532270ea0d74198ab28aa7bc/XStatic-JQuery.TableSorter-2.14.5.2.tar.gz
BuildArch:	noarch


%description
JQuery.TableSorter JavaScript library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package -n python3-XStatic-JQuery-TableSorter
Summary:	JQuery.TableSorter 2.14.5 (XStatic packaging standard)
Provides:	python-XStatic-JQuery-TableSorter
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-XStatic-JQuery-TableSorter
JQuery.TableSorter JavaScript library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package help
Summary:	Development documents and examples for XStatic-JQuery.TableSorter
Provides:	python3-XStatic-JQuery-TableSorter-doc
%description help
JQuery.TableSorter JavaScript library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%prep
%autosetup -n XStatic-JQuery.TableSorter-2.14.5.2

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-XStatic-JQuery-TableSorter -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Sat Jan 30 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
